package com.hrd.demo.controller;

import com.hrd.demo.repository.model.Article;
import com.hrd.demo.repository.model.ArticleFilter;
import com.hrd.demo.service.ArticleServiceImp;
import com.hrd.demo.service.articleService.ArticleService;
import com.hrd.demo.service.articleService.CategoryService;
import com.hrd.demo.utility.Paginate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.Binding;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;


@Controller
public class ArticleController {
    String images;
    @Autowired
    ArticleService articleService;
    @Autowired
    CategoryService categoryService;


    @RequestMapping("/")
    public String getIndex(ModelMap modelMap, @RequestParam(defaultValue = "10") int limit,
                           @RequestParam(defaultValue = "1") int page) {
        Paginate paginate = ((ArticleServiceImp) articleService).getPaginate();

        modelMap.addAttribute("articles", articleService.findAll());
        modelMap.addAttribute("articles_page", articleService.findAllFilter(limit, page, articleFilter));
        modelMap.addAttribute("current_page", paginate.getCurrentPage());
        modelMap.addAttribute("total_page", paginate.getTotalPage());

        modelMap.addAttribute("categories", categoryService.findAll());
        modelMap.addAttribute("total_articles", articleService.counter(articleFilter));

        return "index";
    }


    @RequestMapping("/add")
    public String add(ModelMap modelMap) {


        Article article = new Article();
        article.setId(articleService.findAll().size() + 1);
        modelMap.addAttribute("article", article);
        modelMap.addAttribute("categories", categoryService.findAll());


        return "form";
    }

    @PostMapping("/add")
    public String addAction( @Valid  @ModelAttribute Article article, BindingResult bindingResult,ModelMap modelMap, @RequestParam MultipartFile file) {

        if(bindingResult.hasErrors()){
            modelMap.addAttribute("categories", categoryService.findAll());
            System.out.println("Error");
            return "form";
        }


        configFileName(article, file);

        articleService.add(article);

        return "redirect:/";
    }

    @GetMapping("/article/delete/{id}")
    public String delete(@PathVariable int id) {
        articleService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/article/view/{id}")
    public String view(ModelMap modelMap, @PathVariable int id) {
        modelMap.addAttribute("article", articleService.view(id));
        modelMap.addAttribute("categories", categoryService.findAll());
        return "view";
    }

    @PostMapping("/article/view/{id}")
    public String viewAction(@ModelAttribute Article article) {
        articleService.view(article.getId());
        return "redirect:/view";
    }


    @GetMapping("/article/update/{id}")
    public String update(ModelMap modelMap, @PathVariable int id) {
        images = articleService.view(id).getImage();
        modelMap.addAttribute("article", articleService.view(id));
        modelMap.addAttribute("categories", categoryService.findAll());
        return "update";
    }

    @PostMapping("/article/update")
    public String updateAction(@ModelAttribute Article article, @RequestParam MultipartFile file) {
        configFileName(article, file);
        articleService.update(article);
        return "redirect:/";
    }

    ArticleFilter articleFilter = new ArticleFilter();

    @PostMapping("/article/search")
    public String searchAction(@RequestParam("searcher") String searcher, @RequestParam("cate_id") int cate_id) {

        if (searcher == null & cate_id == 0) {
            articleFilter.setCate_id(null);
            articleFilter.setTitle(null);
        } else {
            articleFilter.setCate_id(cate_id);
            articleFilter.setTitle(searcher);
        }

        return "redirect:/";
    }


    private void configFileName(Article article, MultipartFile file) {
        String fileName = UUID.randomUUID().toString();
        String extension;

        if (!file.isEmpty()) {

            extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
            fileName += extension;
            try {
                Files.copy(file.getInputStream(), Paths.get("src/main/resources/static/img/" + fileName));
                article.setImage(fileName);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (images != null) {
            article.setImage(images);
            images = null;
        } else
            article.setImage("default.jpg");
    }


}


