package com.hrd.demo.controller;

import com.hrd.demo.repository.model.Category;
import com.hrd.demo.service.articleService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping("/category")
    public String getCategoryIndex(ModelMap modelMap) {
        modelMap.addAttribute("categories", categoryService.findAll());

        return "category-index";
    }

    Category category = new Category();

    @RequestMapping("/addCategory")
    public String addCategory(ModelMap modelMap) {
        category.setId(categoryService.findAll().size() + 1);
        modelMap.addAttribute("category", category);

        return "add-category";
    }

    @PostMapping("/addCategory")
    public String add(@Valid @ModelAttribute Category category, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return "add-category";
        }

        categoryService.insert(category);
        return "redirect:/category";
    }

    @GetMapping("/category/delete/{id}")
    public String deleteCategory(@PathVariable int id) {
        categoryService.delete(id);
        return "redirect:/category";
    }

    @GetMapping("/category/update/{id}")
    public String viewCategory(@PathVariable int id, ModelMap modelMap) {

        modelMap.addAttribute("category", categoryService.findOne(id));

        return "update-category";
    }

    @PostMapping("/category/update")
    public String viewAction(@ModelAttribute Category category) {
        categoryService.update(category);
        return "redirect:/category";
    }


}
