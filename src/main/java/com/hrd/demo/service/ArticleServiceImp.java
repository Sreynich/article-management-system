package com.hrd.demo.service;


import com.hrd.demo.repository.articleRepository.ArticleH2Repository;
import com.hrd.demo.repository.model.Article;
import com.hrd.demo.repository.model.ArticleFilter;
import com.hrd.demo.service.articleService.ArticleService;
import com.hrd.demo.utility.Paginate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService {

    private Paginate paginate = new Paginate();


    @Autowired
    ArticleH2Repository articleH2Repository;

    @Override
    public void add(Article article) {
        articleH2Repository.add(article);
    }

    @Override
    public void delete(int id) {
        articleH2Repository.delete(id);
    }

    @Override
    public void update(Article article) {

        articleH2Repository.update(article);
    }

    @Override
    public Article view(int id) {
        return articleH2Repository.view(id);
    }

    @Override
    public List<Article> findAll() {
        return articleH2Repository.findAll();
    }


    @Override
    public List<Article> findAllFilter(int limit, int page, ArticleFilter articleFilter) {
        int size = articleH2Repository.findAll().size();
        int startIndex = (page - 1) * limit;
        int endIndex = startIndex + limit;
        int totalPage= (int)Math.ceil(size / (double)limit);

        paginate.setCurrentPage(page);
        paginate.setTotalPage(totalPage);
        return articleH2Repository.findAllFilter(articleFilter,limit,startIndex);
    }

    @Override
    public Integer counter(ArticleFilter filter) {
        return articleH2Repository.counter(filter);
    }


    public Paginate getPaginate() {
        return paginate;
    }

}
