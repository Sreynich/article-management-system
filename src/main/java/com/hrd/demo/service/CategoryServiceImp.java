package com.hrd.demo.service;

import com.hrd.demo.repository.articleRepository.CategoryH2Repository;
import com.hrd.demo.repository.model.Category;
import com.hrd.demo.service.articleService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    CategoryH2Repository categoryH2Repository;


    @Override
    public void delete(int id) {
        categoryH2Repository.delete(id);
    }

    @Override
    public Category findOne(int id) {
        return categoryH2Repository.findOne(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryH2Repository.findAll();
    }

    @Override
    public void insert(Category category) {
        categoryH2Repository.insert(category);
    }

    @Override
    public void update(Category category) {
        categoryH2Repository.update(category);
    }
}


