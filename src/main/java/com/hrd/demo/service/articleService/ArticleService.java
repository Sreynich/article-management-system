package com.hrd.demo.service.articleService;

import com.hrd.demo.repository.model.Article;
import com.hrd.demo.repository.model.ArticleFilter;

import java.util.List;

public interface ArticleService {

    void add(Article article);
    void delete( int id);
    void update( Article article);
    Article view( int id);
    List<Article> findAll();
    List<Article> findAllFilter(int limit, int page, ArticleFilter articleFilter);
    Integer counter(ArticleFilter filter);
}
