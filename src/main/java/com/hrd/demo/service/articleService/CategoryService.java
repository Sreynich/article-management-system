package com.hrd.demo.service.articleService;

import com.hrd.demo.repository.model.Category;

import java.util.List;

public interface CategoryService {
    void delete(int id);
    Category findOne(int id);
    List<Category> findAll();
    void insert(Category category);
    void update(Category category);

}
