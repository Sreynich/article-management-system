package com.hrd.demo.repository.articleRepository;


import com.hrd.demo.repository.model.Category;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryH2Repository {

@Select("select * from tb_category order by category_id asc")
@Results({
        @Result(property = "id", column = "category_id"),
        @Result(property = "title", column = "category_title")
})
List<Category> findAll();

@Select("SELECT * FROM TB_CATEGORY  where category_id=#{id}")
@Results({
        @Result(property = "id", column = "category_id"),
        @Result(property = "title", column = "category_title")
})
Category findOne( int id);

@Delete("delete from tb_category where category_id=#{id}")
void delete(int id);

@Update("update tb_category set category_title= #{title} where category_id=#{id} ")
void update ( Category category);

@Insert("insert into tb_category (category_title) values (#{title})")
void insert(Category category);


}

