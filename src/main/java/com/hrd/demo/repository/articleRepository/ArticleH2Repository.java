package com.hrd.demo.repository.articleRepository;

import com.hrd.demo.repository.model.Article;
import com.hrd.demo.repository.model.ArticleFilter;
import com.hrd.demo.repository.provider.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleH2Repository {

    @Select("Select * from tb_articles a inner join tb_category c on c.category_id=a.category_id")
    @Results({
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.title", column = "category_title"),
            @Result(property = "desc", column = "descr")
    })
    List<Article> findAll();

    @Insert("Insert into tb_articles (title, author, image,descr , category_id) values (#{title}, " +
            "#{author}, #{image},#{desc}, #{category.id})")
    void add(Article article);

    @Delete("Delete from tb_articles where id=#{id}")
    void delete(int id);


    @Select("select * from tb_articles where id=#{id}")
    @Results({
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.title", column = "category_title"),
            @Result(property = "desc", column = "descr")
    })
    Article view(int id);

    @Update("update tb_articles set  title=#{title}, author=#{author}, descr=#{desc},image=#{image}, category_id=#{category.id}  where id=#{id}")
    void update(Article article);


    @SelectProvider(method = "findAllFilter", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.id", column = "category_id"),
            @Result(property = "category.title", column = "category_title"),
            @Result(property = "desc", column = "descr")
    })
    List<Article> findAllFilter(@Param("article") ArticleFilter articleFilter, @Param("limit") int limit, @Param("offset") int offset);

    @SelectProvider(method = "countFilter", type = ArticleProvider.class)
    Integer counter(@Param("filter") ArticleFilter filter);
}
