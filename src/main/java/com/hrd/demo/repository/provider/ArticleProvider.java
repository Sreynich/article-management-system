package com.hrd.demo.repository.provider;

import com.hrd.demo.repository.model.ArticleFilter;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAllFilter(@Param("article") ArticleFilter articleFilter, @Param("limit") int limit, @Param("offset") int offset) {
        return new SQL() {{
            SELECT("a.*,c.category_title");
            FROM("tb_articles  a");
            INNER_JOIN(" tb_category c on c.category_id=a.category_id");
            if (articleFilter.getTitle() != null && articleFilter.getTitle() != "") {
                WHERE("a.title ILIKE '%' || #{article.title} || '%' ");
            }

            if (articleFilter.getCate_id() != null && articleFilter.getCate_id() != 0) {
                WHERE("a.category_id=#{article.cate_id}");
            }

            ORDER_BY("a.id limit #{limit} offset #{offset} ");
        }}.toString();
    }

    public String countFilter(@Param("filter") ArticleFilter articleFilter) {
        return new SQL() {{
            SELECT("COUNT(a.id)");
            FROM("tb_articles  a");
            if (articleFilter.getTitle() != null && !articleFilter.getTitle().equals("")) {
                WHERE("a.title ILIKE '%' || #{filter.title} || '%' ");
            }

            if (articleFilter.getCate_id() != null && articleFilter.getCate_id() != 0) {
                WHERE("a.category_id=#{filter.cate_id}");
            }

//            ORDER_BY("a.id limit #{limit} offset #{offset} ");
        }}.toString();
    }
}
