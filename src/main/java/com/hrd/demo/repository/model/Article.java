package com.hrd.demo.repository.model;

import javax.validation.constraints.NotEmpty;

public class Article {
    private int id;
    @NotEmpty(message = "{title-error}")
    private String title;
    @NotEmpty(message = "{author-error}")
    private String author;
    private String image;
    @NotEmpty(message = "{desc-error}")
    private String desc;
    private Category category;
    public Article( ){

    }


    public Article(int id, String title, String author, String image, String desc, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.image = image;
        this.desc = desc;
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", image='" + image + '\'' +
                ", desc='" + desc + '\'' +
                ", category=" + category +
                '}';
    }
}
